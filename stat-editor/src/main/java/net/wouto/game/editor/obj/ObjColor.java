package net.wouto.game.editor.obj;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.scene.paint.Color;

public class ObjColor {

	private int red;
	private int green;
	private int blue;

	public ObjColor(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	@JsonIgnore
	public Color getPaint() {
		return new Color(this.getRedDouble(), this.getGreenDouble(), this.getBlueDouble(), 1.0d);
	}

	public Color getPaintMod(int brightness) {
		Color c = getPaint();
		boolean darken = false;
		if (brightness < 0) {
			brightness *= -1;
			darken = true;
		}
		for (int i = 0; i < brightness; i++) {
			if (darken) {
				c = c.darker();
			} else {
				c = c.brighter();
			}
		}
		return c;
	}

	public ObjColor() {
		this(0, 0, 0);
	}

	@JsonIgnore
	public double getRedDouble() {
		return (((double)this.red) / 255.0d);
	}

	@JsonIgnore
	public double getGreenDouble() {
		return (((double)this.green) / 255.0d);
	}

	@JsonIgnore
	public double getBlueDouble() {
		return (((double)this.blue) / 255.0d);
	}

	private void fix() {
		this.red = this.red > 255 ? 255 : this.red < 0 ? 0 : this.red;
		this.green = this.green > 255 ? 255 : this.green < 0 ? 0 : this.green;
		this.blue = this.blue > 255 ? 255 : this.blue < 0 ? 0 : this.blue;
	}

	public void set(int r, int g, int b) {
		this.red = r;
		this.green = g;
		this.blue = b;
		this.fix();
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
		this.fix();
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
		this.fix();
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
		this.fix();
	}

	@Override
	public String toString() {
		return "ObjColor{" +
				"red=" + red +
				", green=" + green +
				", blue=" + blue +
				'}';
	}
}

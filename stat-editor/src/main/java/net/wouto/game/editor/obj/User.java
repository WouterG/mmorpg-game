package net.wouto.game.editor.obj;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

public class User {

	private String username;
	private String email;

	@JsonSerialize(using = UserFlagSerializer.Serializer.class)
	@JsonDeserialize(using = UserFlagSerializer.Deserializer.class)
	private List<UserFlag> flags;
	private boolean admin;

	public User() {
		this.flags = new ArrayList<>();
	}

	public User(String username) {
		this();
		this.username = username;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<UserFlag> getFlags() {
		return flags;
	}

	public void setFlags(List<UserFlag> flags) {
		if (flags == null) {
			return;
		}
		this.flags = new ArrayList<>(flags);
	}
}

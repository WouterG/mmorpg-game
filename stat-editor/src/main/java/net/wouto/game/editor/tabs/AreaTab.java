package net.wouto.game.editor.tabs;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.wouto.game.editor.json.JsonLoader;
import net.wouto.game.editor.obj.ButtonResult;
import net.wouto.game.editor.obj.GameArea;
import net.wouto.game.editor.tabs.table.AreaTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AreaTab extends SplitPane {

	private AreaTable areaTable;

	private ScrollPane optionsPane;

	private GridPane optionsContent;

	public AreaTab() {
		try {
			this.initialize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initialize() throws IOException {
		List<GameArea> areas = JsonLoader.loadItems(GameArea.class, "game-areas");
		SimpleListProperty<GameArea> areasProperty = new SimpleListProperty<GameArea>(FXCollections.observableArrayList(areas));
		this.areaTable = new AreaTable(areasProperty);
		this.optionsPane = new ScrollPane();
		this.optionsContent = new GridPane();
		this.optionsPane.setFitToHeight(true);
		this.optionsPane.setFitToWidth(true);
		this.optionsContent.setVgap(5.0d);
		this.optionsContent.setHgap(5.0d);
		this.optionsContent.setAlignment(Pos.CENTER);
		this.optionsContent.setPadding(new Insets(10d));
		this.optionsPane.setContent(this.optionsContent);

		this.optionsContent.add(new Label("Area Name"), 0, 1);
		this.optionsContent.add(new Label("Game Ages"), 0, 2);

		Button agesEditButton = new Button("Edit Ages");
		agesEditButton.setOnMouseClicked(event -> {
			GameArea area = this.areaTable.getSelectionModel().getSelectedItem();
			if (event.getButton() == MouseButton.PRIMARY && area != null) {
				Stage stage = new Stage(StageStyle.UTILITY);
				AgesEditPane tab = new AgesEditPane(area.getAges(), stage);
				Scene scene = new Scene(tab, 1400, 800);
				stage.setScene(scene);
				stage.getIcons().add(new Image(AreaTab.class.getResourceAsStream("/program-icon.png")));
				stage.showAndWait();
				if (tab.getResult() == ButtonResult.ACCEPT) {
					area.getAges().clear();
					area.getAges().addAll(tab.getAgesTable().getItems());
					areaTable.refresh();
				}
			}
		});

		this.optionsContent.add(agesEditButton, 1, 2);

		TextField areaName = new TextField();

		this.optionsContent.add(areaName, 1, 1);

		GridPane buttonBar = new GridPane();
		buttonBar.setHgap(5.0d);

		Button deleteButton = new Button("Delete");
		Button addButton = new Button("Add");
		Button saveButton = new Button("Save");

		deleteButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				GameArea area = areaTable.getSelectionModel().getSelectedItem();
				if (area != null) {
					areaTable.getItems().remove(area);
				}
			}
		});

		addButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				GameArea area = new GameArea("new area");
				areaTable.getItems().add(area);
				areaTable.getSelectionModel().select(area);
			}
		});

		saveButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				GameArea area = areaTable.getSelectionModel().getSelectedItem();
				if (area != null) {
					area.setName(areaName.getText());
					areaTable.refresh();
				}
				ArrayList<GameArea> data = new ArrayList<>(areaTable.getItems());
				try {
					JsonLoader.saveItems(data, "game-areas");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		this.areaTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				areaName.setText(newValue.getName());
			}
		});

		buttonBar.addRow(0, deleteButton, addButton, saveButton);

		this.optionsContent.add(buttonBar, 0, 0, 2, 1);

		this.getItems().setAll(areaTable, this.optionsPane);
	}
}

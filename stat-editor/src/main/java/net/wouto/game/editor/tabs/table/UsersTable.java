package net.wouto.game.editor.tabs.table;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import net.wouto.game.editor.obj.User;
import net.wouto.game.editor.obj.UserFlag;

import java.util.ArrayList;
import java.util.List;

public class UsersTable extends TableView<User> {

	public UsersTable() {
		this.initialize();
	}

	public UsersTable(ObservableList<User> items) {
		super(items);
		this.initialize();
	}

	private void initialize() {
		TableColumn<User, String> username = new TableColumn<>();
		username.setText("Username");
		username.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getUsername()));

		TableColumn<User, String> email = new TableColumn<>();
		email.setText("Email");
		email.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEmail()));

		TableColumn<User, User> flagsCol = new TableColumn<>();
		flagsCol.setText("Flags");
		flagsCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue()));
		flagsCol.setCellFactory(new Callback<TableColumn<User, User>, TableCell<User, User>>() {
			@Override
			public TableCell<User, User> call(TableColumn<User, User> param) {
				TableCell<User, User> cell = new TableCell<User, User>() {
					@Override
					protected void updateItem(User item, boolean empty) {
						if (item == null || empty) {
							setText("");
							super.updateItem(item, empty);
							return;
						}
						if (item.getFlags().isEmpty()) {
							setText(" - ");
						} else {
							List<String> flagNames = new ArrayList<>();
							for (UserFlag userFlag : item.getFlags()) {
								flagNames.add(userFlag.getCodeName());
							}
							setText(String.join(", ", flagNames));
						}
						super.updateItem(item, empty);
					}
				};
				return cell;
			}
		});

		TableColumn<User, String> admin = new TableColumn<>();
		admin.setText("Admin");
		admin.setCellValueFactory(param ->
				new SimpleObjectProperty<>(
						param.getValue().isAdmin() ? "Yes" : ""
				)
		);

		this.getColumns().addAll(username, email, admin, flagsCol);
	}
}

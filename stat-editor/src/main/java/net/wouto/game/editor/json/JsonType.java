package net.wouto.game.editor.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class JsonType {

	private Class<?> type;
	private Class<?> typeInterface;
	private List<JsonType> parametric;

	private JsonType() {
		this.parametric = new ArrayList<>();
	}

	/**
	 * <p>Create a new JsonType reference with just a type.</p>
	 * <p>using {@link String#getClass()} as parameter results in a simple String type instnace</p>
	 * @param type The type this JsonType should deserialize and return as
	 */
	public JsonType(Class<?> type) {
		this();
		this.type = type;
	}

	/**
	 * <p>Create a new JsonType reference with a type and a type interface.</p>
	 * <p>Used when parsing an object as an implementation of an interface or abstract class and you want it to be deserialized
	 * as the implementation but returned as interface.</p>
	 * as the implementation but returns as interface.</p>
	 * @param type The type this JsonType should deserialize as
	 * @param typeInterface The type this JsonType should return as
	 */
	public JsonType(Class<?> type, Class<?> typeInterface) {
		this(type);
		this.typeInterface = typeInterface;
	}

	/**
	 * <p>Create a new JsonType reference with a type and generic parameters</p>
	 * <p>The type can be classes that have a generic parameter like {@link HashMap} or {@link ArrayList}.</p>
	 * <p>The parametric types are the generic parameters of the type.</p>
	 * <p>Example: using {@link HashMap} as type and parametric values:
	 *      {@link String} and {@link Integer}
	 *      creates a {@code HashMap<String, Integer>}
	 * </p>
	 * @param type The type this JsonType should deserialize and return as
	 * @param parametric The generic parameter(s) for the type
	 */
	public JsonType(Class<?> type, JsonType... parametric) {
		this(type);
		this.parametric.addAll(Arrays.asList(parametric));
	}

	/**
	 * <p>Create a new JsonType reference with a type, type interface and generic parameters</p>
	 * <p>Used when parsing an object as an implementation of an interface or abstract class and you want it to be deserialized
	 * as the implementation but returned as interface.</p>
	 * <p>The parametric types are the generic parameters of the type.</p>
	 * <p>Example: using {@link HashMap} as type and parametric values:
	 *      {@link String} and {@link Integer}
	 *      creates a {@code HashMap<String, Integer>}
	 * </p>
	 * @param type The type this JsonType should deserialize as
	 * @param typeInterface The type this JsonType should return as
	 * @param parametric The generic parameter(s) for the type
	 */
	public JsonType(Class<?> type, Class<?> typeInterface, JsonType... parametric) {
		this(type, parametric);
		this.typeInterface = typeInterface;
	}

	/**
	 * <p>Create a new JsonType reference with a type and generic parameters</p>
	 * <p>The type can be classes that have a generic parameter like {@link HashMap} or {@link ArrayList}.</p>
	 * <p>The parametric types are the generic parameters of the type.</p>
	 * <p>Example: using {@link HashMap} as type and parametric values:
	 *      {@link String} and {@link Integer}
	 *      creates a {@code HashMap<String, Integer>}
	 * </p>
	 * @param type The type this JsonType should deserialize and return as
	 * @param parametric The generic parameter(s) for the type
	 */
	public JsonType(Class<?> type, Class<?>... parametric) {
		this(type);
		for (Class para : parametric) {
			this.parametric.add(new JsonType(para));
		}
	}

	/**
	 * <p>Create a new JsonType reference with a type, type interface and generic parameters</p>
	 * <p>Used when parsing an object as an implementation of an interface or abstract class and you want it to be deserialized
	 * as the implementation but returned as interface.</p>
	 * <p>The parametric types are the generic parameters of the type.</p>
	 * <p>Example: using {@link HashMap} as type and parametric values:
	 *      {@link String} and {@link Integer}
	 *      creates a {@code HashMap<String, Integer>}
	 * </p>
	 * @param type The type this JsonType should deserialize as
	 * @param typeInterface The type this JsonType should return as
	 * @param parametric The generic parameter(s) for the type
	 */
	public JsonType(Class<?> type, Class<?> typeInterface, Class<?>... parametric) {
		this(type, parametric);
		this.typeInterface = typeInterface;
	}

	public Class getType() {
		return type;
	}

	public Class getTypeInterface() {
		if (this.typeInterface == null) {
			return this.type;
		}
		return typeInterface;
	}

	public List<JsonType> getParametric() {
		return parametric;
	}
}
package net.wouto.game.editor.tabs.table;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.wouto.game.editor.obj.GameAge;

public class AgesTable extends TableView<GameAge> {

	public AgesTable() {
		this.initialize();
	}

	public AgesTable(ObservableList<GameAge> items) {
		super(items);
		this.initialize();
	}

	@SuppressWarnings("unchecked")
	private void initialize() {
		TableColumn<GameAge, String> name = new TableColumn<>();
		name.setText("Name");
		name.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getName()));

		TableColumn<GameAge, Integer> startYear = new TableColumn<>();
		startYear.setText("Start Year");
		startYear.setSortType(TableColumn.SortType.ASCENDING);
		startYear.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getStartYear()));

		TableColumn<GameAge, Integer> endYear = new TableColumn<>();
		endYear.setText("End Year");
		endYear.setSortType(TableColumn.SortType.ASCENDING);
		endYear.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEndYear()));

		getSortOrder().addAll(startYear, endYear);

		this.getColumns().addAll(name, startYear, endYear);
	}

}

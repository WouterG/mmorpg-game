package net.wouto.game.editor;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import net.wouto.game.editor.tabs.AreaTab;
import net.wouto.game.editor.tabs.FlagsTab;
import net.wouto.game.editor.tabs.UsersTab;
import net.wouto.game.editor.tabs.table.FlagsTable;

import java.io.IOException;

public class GameEditor extends Application {

	public static GameEditor getInstance() {
		return instance;
	}

	private static GameEditor instance;

	private BorderPane mainPane;

	private TabPane mainTabPane;

	private Tab ages;

	private Tab buildings;

	private Tab units;

	private Tab flags;

	private Tab users;

	private Tab areas;

	private FlagsTable flagsTable;

	public static void main(String[] args) {
		launch(args);
	}

	public FlagsTable getFlagsTable() {
		return flagsTable;
	}

	@Override
	public void start(Stage stage) throws IOException {
		instance = this;
		stage.getIcons().clear();
		stage.getIcons().add(new Image(GameEditor.class.getResourceAsStream("/program-icon.png")));
		mainPane = new BorderPane();
		mainPane.setPrefSize(1400, 800);
		mainTabPane = new TabPane();
		mainTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
		mainPane.setCenter(mainTabPane);
		Scene scene = new Scene(mainPane);

		//ages = new Tab("Ages Editor", new AgesEditPane());
		//buildings = new Tab("Buildings Editor");
		//units = new Tab("Units Editor");
		FlagsTab flagsTab = new FlagsTab();
		flagsTable = flagsTab.getFlagsTable();
		flags = new Tab("Flags Editor", flagsTab);
		users = new Tab("User Editor", new UsersTab());
		areas = new Tab("Area Editor", new AreaTab());

		mainTabPane.getTabs().addAll(flags, users, areas);

		stage.setTitle("Game Editor");
		stage.setScene(scene);
		stage.show();
	}

}

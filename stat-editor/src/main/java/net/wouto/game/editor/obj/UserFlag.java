package net.wouto.game.editor.obj;

public class UserFlag {

	private String codeName;

	private String displayName;

	private ObjColor colorBack;

	private ObjColor colorFront;

	private boolean publicVisible;

	public UserFlag(String codeName, String displayName, ObjColor colorBack, ObjColor colorFront) {
		this.codeName = codeName;
		this.displayName = displayName;
		this.colorBack = colorBack;
		this.colorFront = colorFront;
		this.publicVisible = false;
	}

	public boolean getPublicVisible() {
		return publicVisible;
	}

	public void setPublicVisible(boolean publicVisible) {
		this.publicVisible = publicVisible;
	}

	public UserFlag() {
		this(null, "", new ObjColor(), new ObjColor());
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public ObjColor getColorBack() {
		return colorBack;
	}

	public void setColorBack(ObjColor colorBack) {
		this.colorBack = colorBack;
	}

	public ObjColor getColorFront() {
		return colorFront;
	}

	public void setColorFront(ObjColor colorFront) {
		this.colorFront = colorFront;
	}

	@Override
	public String toString() {
		return "UserFlag{" +
				"codeName='" + codeName + '\'' +
				", displayName='" + displayName + '\'' +
				", colorBack=" + colorBack +
				", colorFront=" + colorFront +
				'}';
	}
}

package net.wouto.game.editor.tabs;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import net.wouto.game.editor.GameEditor;
import net.wouto.game.editor.json.JsonLoader;
import net.wouto.game.editor.obj.User;
import net.wouto.game.editor.obj.UserFlag;
import net.wouto.game.editor.tabs.input.SelectionListEditor;
import net.wouto.game.editor.tabs.pane.UserPreviewPane;
import net.wouto.game.editor.tabs.table.UsersTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UsersTab extends SplitPane {

	private UsersTable usersTable;

	private ScrollPane optionsPane;

	private GridPane optionsContent;

	private UserPreviewPane userPreviewPane;

	public UsersTab() {
		try {
			this.initialize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initialize() throws IOException {
		this.userPreviewPane = new UserPreviewPane(null);
		List<User> flags = JsonLoader.loadItems(User.class, "users");
		this.usersTable = new UsersTable(FXCollections.observableArrayList(flags));
		this.optionsPane = new ScrollPane();
		this.optionsContent = new GridPane();
		this.optionsPane.setFitToHeight(true);
		this.optionsPane.setFitToWidth(true);
		this.optionsContent.setVgap(5.0d);
		this.optionsContent.setHgap(5.0d);
		this.optionsContent.setAlignment(Pos.CENTER);
		this.optionsContent.setPadding(new Insets(10d));
		this.optionsPane.setContent(this.optionsContent);

		this.optionsContent.add(new Label("Username"), 0, 1);
		this.optionsContent.add(new Label("Email"), 0, 2);
		this.optionsContent.add(new Label("Admin"), 0, 3);
		this.optionsContent.add(new Label("Flags"), 0, 4);

		TextField usernameField = new TextField();
		TextField emailField = new TextField();
		CheckBox adminField = new CheckBox();
		List<UserFlag> flagOptions = new ArrayList<>();
		flagOptions.addAll(GameEditor.getInstance().getFlagsTable().getItems());
		SelectionListEditor<UserFlag> flagSelection = new SelectionListEditor<>(flagOptions, UserFlag::getCodeName);

		this.optionsContent.add(usernameField, 1, 1);
		this.optionsContent.add(emailField, 1, 2);
		this.optionsContent.add(adminField, 1, 3);
		this.optionsContent.add(flagSelection, 0, 5, 2, 1);
		this.optionsContent.add(userPreviewPane, 0, 6, 2, 1);

		GridPane buttonBar = new GridPane();
		buttonBar.setHgap(5.0d);

		Button deleteButton = new Button("Delete");
		Button addButton = new Button("Add");
		Button saveButton = new Button("Save");

		deleteButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				User user = usersTable.getSelectionModel().getSelectedItem();
				if (user != null) {
					usersTable.getItems().remove(user);
				}
			}
		});

		addButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				User user = new User("New User");
				user.setAdmin(false);
				user.setEmail("");
				usersTable.getItems().add(user);
				usersTable.getSelectionModel().select(user);
			}
		});

		saveButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				User user = usersTable.getSelectionModel().getSelectedItem();
				if (user != null) {
					user.setUsername(usernameField.getText());
					user.setEmail(emailField.getText());
					user.setAdmin(adminField.isSelected());
					user.setFlags(flagSelection.getSelectedList());
					userPreviewPane.valueProperty().set(user);
					userPreviewPane.update();
					usersTable.refresh();
				}
				ArrayList<User> data = new ArrayList<>(usersTable.getItems());
				try {
					JsonLoader.saveItems(data, "users");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		this.usersTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				usernameField.setText(newValue.getUsername());
				emailField.setText(newValue.getEmail());
				adminField.setSelected(newValue.isAdmin());
				flagSelection.getSelectedList().setAll(newValue.getFlags());
				userPreviewPane.valueProperty().set(newValue);
			}
		});

		buttonBar.addRow(0, deleteButton, addButton, saveButton);

		this.optionsContent.add(buttonBar, 0, 0, 2, 1);


		this.getItems().setAll(usersTable, this.optionsPane);
	}

}

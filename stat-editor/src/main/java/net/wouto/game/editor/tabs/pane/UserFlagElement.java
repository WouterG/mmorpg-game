package net.wouto.game.editor.tabs.pane;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import net.wouto.game.editor.obj.UserFlag;

public class UserFlagElement extends Label {

	private UserFlag flag;

	public UserFlagElement(UserFlag flag) {
		this.flag = flag;
		this.initialize();
	}

	public void setUserFlag(UserFlag flag) {
		this.flag = flag;
		this.initialize();
	}

	private void initialize() {
		this.setBorder(new Border(new BorderStroke(
				flag.getColorBack().getPaintMod(-2),
				BorderStrokeStyle.SOLID,
				new CornerRadii(5d),
				new BorderWidths(1.5d),
				new Insets(4d)
		)));
		this.setFont(Font.font("Arial", FontWeight.EXTRA_BOLD, 20.0d));
		this.setPadding(new Insets(4d));
		this.setBackground(new Background(new BackgroundFill(
				new LinearGradient(
						0, 0, 0, 1,
						true,
						CycleMethod.NO_CYCLE,
						new Stop(0, this.flag.getColorBack().getPaintMod(1)),
						new Stop(1, this.flag.getColorBack().getPaintMod(-1))),
				new CornerRadii(5d),
				new Insets(4d)
		)));
		this.setTextFill(this.flag.getColorFront().getPaint());
		this.setText(this.flag.getDisplayName());
	}

}

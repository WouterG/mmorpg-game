package net.wouto.game.editor.tabs.input;

import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;
import net.wouto.game.editor.obj.ObjColor;

public class ObjColorInputPane extends GridPane {

	private ObjColor value;

	private Spinner<Integer> red;

	private Spinner<Integer> green;

	private Spinner<Integer> blue;

	public ObjColorInputPane() {
		this.red = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 255));
		this.green = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 255));
		this.blue = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 255));

		this.red.setEditable(true);
		this.green.setEditable(true);
		this.blue.setEditable(true);

		this.setHgap(5d);

		this.red.valueProperty().addListener((observable, oldValue, newValue) -> {
			this.value.setRed(newValue);
		});
		this.green.valueProperty().addListener((observable, oldValue, newValue) -> {
			this.value.setGreen(newValue);
		});
		this.blue.valueProperty().addListener((observable, oldValue, newValue) -> {
			this.value.setBlue(newValue);
		});

		this.red.getEditor().textProperty().addListener((observable, oldValue, newValue) -> commitEditorText(red));
		this.green.getEditor().textProperty().addListener((observable, oldValue, newValue) -> commitEditorText(green));
		this.blue.getEditor().textProperty().addListener((observable, oldValue, newValue) -> commitEditorText(blue));

		this.setValue(new ObjColor());
		this.add(this.red, 0, 0);
		this.add(this.green, 1, 0);
		this.add(this.blue, 2, 0);
	}

	private <T> void commitEditorText(Spinner<T> spinner) {
		try {
			if (!spinner.isEditable()) return;
			String text = spinner.getEditor().getText();
			SpinnerValueFactory<T> valueFactory = spinner.getValueFactory();
			if (valueFactory != null) {
				StringConverter<T> converter = valueFactory.getConverter();
				if (converter != null) {
					T value = converter.fromString(text);
					valueFactory.setValue(value);
				}
			}
		} catch (Exception ignored) {
		}
	}

	private void setValue(Spinner<Integer> spinner, int value) {
		spinner.getValueFactory().setValue(value);
	}

	public void setValue(ObjColor value) {
		this.value = value;
		this.setValue(this.red, this.value.getRed());
		this.setValue(this.green, this.value.getGreen());
		this.setValue(this.blue, this.value.getBlue());
	}

	public ObjColor getValue() {
		return new ObjColor(this.value.getRed(), this.value.getGreen(), this.value.getBlue());
	}

}

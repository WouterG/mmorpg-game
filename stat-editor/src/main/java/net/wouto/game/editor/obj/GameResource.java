package net.wouto.game.editor.obj;

public class GameResource {

	private String name;

	public GameResource(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

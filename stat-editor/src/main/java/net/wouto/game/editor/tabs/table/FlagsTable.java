package net.wouto.game.editor.tabs.table;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import net.wouto.game.editor.obj.UserFlag;

public class FlagsTable extends TableView<UserFlag> {

	public FlagsTable() {
		this.initialize();
	}

	public FlagsTable(ObservableList<UserFlag> items) {
		super(items);
		this.initialize();
	}

	private void initialize() {
		TableColumn<UserFlag, String> codeName = new TableColumn<>();
		codeName.setText("Code Name");
		codeName.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getCodeName()));

		TableColumn<UserFlag, String> displayName = new TableColumn<>();
		displayName.setText("Display Name");
		displayName.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getDisplayName()));

		TableColumn<UserFlag, UserFlag> previewCol = new TableColumn<>();
		previewCol.setText("Preview");
		previewCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue()));
		previewCol.setCellFactory(new Callback<TableColumn<UserFlag, UserFlag>, TableCell<UserFlag, UserFlag>>() {
			@Override
			public TableCell<UserFlag, UserFlag> call(TableColumn<UserFlag, UserFlag> param) {
				TableCell<UserFlag, UserFlag> cell = new TableCell<UserFlag, UserFlag>() {
					@Override
					protected void updateItem(UserFlag item, boolean empty) {
						if (item == null || empty) {
							setText("");
							setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
							setTextFill(Color.WHITE);
							super.updateItem(item, empty);
							return;
						}
						setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, Font.getDefault().getSize()));
						setText(item.getDisplayName());
						setTextFill(new Color(item.getColorFront().getRedDouble(), item.getColorFront().getGreenDouble(), item.getColorFront().getBlueDouble(), 1.0));
						setBackground(new Background(new BackgroundFill(
								new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop(0, item.getColorBack().getPaintMod(1)), new Stop(1, item.getColorBack().getPaintMod(-1))),
								null, null
						)));
						super.updateItem(item, empty);
					}
				};
				cell.setAlignment(Pos.CENTER);
				return cell;
			}
		});

		TableColumn<UserFlag, String> publicVisible = new TableColumn<>();
		publicVisible.setText("Publicly Visible");
		publicVisible.setCellValueFactory(param ->
				new SimpleObjectProperty<>(
						param.getValue().getPublicVisible() ? "Yes" : ""
				)
		);

		this.getColumns().addAll(codeName, displayName, publicVisible, previewCol);
	}

}

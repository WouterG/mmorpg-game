package net.wouto.game.editor.tabs;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import net.wouto.game.editor.obj.ButtonResult;
import net.wouto.game.editor.obj.GameAge;
import net.wouto.game.editor.tabs.table.AgesTable;

import java.io.IOException;
import java.util.List;

public class AgesEditPane extends SplitPane {

	private AgesTable agesTable;

	private ScrollPane optionsPane;

	private Button acceptButton;

	private Button cancelButton;

	private ButtonResult result;

	private Stage stage;

	public AgesEditPane(List<GameAge> ages, Stage stage) {
		this.stage = stage;
		this.result = ButtonResult.CANCEL;
		try {
			this.initialize(ages);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public AgesTable getAgesTable() {
		return agesTable;
	}

	public ButtonResult getResult() {
		return result;
	}

	private void initialize(List<GameAge> ages) throws IOException {
		this.agesTable = new AgesTable(FXCollections.observableArrayList(ages));
		this.optionsPane = new ScrollPane();
		this.optionsPane.setFitToWidth(true);
		this.optionsPane.setFitToHeight(true);
		GridPane optionsContent = new GridPane();
		optionsContent.setAlignment(Pos.CENTER);
		this.optionsPane.setContent(optionsContent);

		this.acceptButton = new Button("Accept/Save");
		this.cancelButton = new Button("Cancel");

		this.acceptButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				result = ButtonResult.ACCEPT;
				stage.close();
			}
		});

		this.cancelButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				result = ButtonResult.CANCEL;
				stage.close();
			}
		});

		TextField name = new TextField();
		TextField startYear = new TextField();
		TextField endYear = new TextField();

		this.agesTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue == null) {
				name.setText("");
				startYear.setText("");
				endYear.setText("");
			} else {
				name.setText(newValue.getName());
				startYear.setText(Integer.toString(newValue.getStartYear()));
				endYear.setText(Integer.toString(newValue.getEndYear()));
			}
		});

		Button addButton = new Button("Add Age");
		Button deleteButton = new Button("Delete Age");
		Button saveButton = new Button("Save Current Age");

		addButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				GameAge age = new GameAge("new age");
				age.setStartYear(Integer.MAX_VALUE - 1);
				age.setEndYear(Integer.MAX_VALUE);
				this.agesTable.getItems().add(age);
			}
		});

		deleteButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				GameAge age = agesTable.getSelectionModel().getSelectedItem();
				if (age != null) {
					agesTable.getSelectionModel().clearSelection();
					agesTable.getItems().remove(age);
					agesTable.refresh();
				}
			}
		});

		saveButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				GameAge age = agesTable.getSelectionModel().getSelectedItem();
				if (age != null) {
					Integer startYearNr = parseInt(startYear.getText());
					Integer endYearNr = parseInt(endYear.getText());
					if (startYearNr == null || endYearNr == null) {
						return;
					}
					age.setName(name.getText());
					age.setStartYear(startYearNr);
					age.setEndYear(endYearNr);
					agesTable.refresh();
				}
			}
		});

		optionsContent.add(new SplitPane(this.acceptButton, this.cancelButton), 0, 0, 2, 1);
		optionsContent.add(new Label("Name"), 0, 1);
		optionsContent.add(new Label("Start Year"), 0, 2);
		optionsContent.add(new Label("End Year"), 0, 3);
		optionsContent.add(name, 1, 1);
		optionsContent.add(startYear, 1, 2);
		optionsContent.add(endYear, 1, 3);
		optionsContent.add(addButton, 0, 5);
		optionsContent.add(deleteButton, 1, 5);
		optionsContent.add(saveButton, 0, 4);

		this.getItems().setAll(agesTable, this.optionsPane);
	}

	private Integer parseInt(String text) {
		try {
			return Integer.parseInt(text);
		} catch (Exception e) {
			return null;
		}
	}

}

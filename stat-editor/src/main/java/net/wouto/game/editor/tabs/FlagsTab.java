package net.wouto.game.editor.tabs;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import net.wouto.game.editor.json.JsonLoader;
import net.wouto.game.editor.obj.ObjColor;
import net.wouto.game.editor.obj.UserFlag;
import net.wouto.game.editor.tabs.input.ObjColorInputPane;
import net.wouto.game.editor.tabs.table.FlagsTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlagsTab extends SplitPane {

	private FlagsTable flagsTable;

	private ScrollPane optionsPane;

	private GridPane optionsContent;

	public FlagsTab() {
		try {
			this.initialize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public FlagsTable getFlagsTable() {
		return flagsTable;
	}

	private void initialize() throws IOException {
		List<UserFlag> flags = JsonLoader.loadItems(UserFlag.class, "user-flags");
		this.flagsTable = new FlagsTable(FXCollections.observableArrayList(flags));
		this.optionsPane = new ScrollPane();
		this.optionsContent = new GridPane();
		this.optionsPane.setFitToHeight(true);
		this.optionsPane.setFitToWidth(true);
		this.optionsContent.setVgap(5.0d);
		this.optionsContent.setHgap(5.0d);
		this.optionsContent.setAlignment(Pos.CENTER);
		this.optionsContent.setPadding(new Insets(10d));
		this.optionsPane.setContent(this.optionsContent);

		this.optionsContent.add(new Label("Code name"), 0, 1);
		this.optionsContent.add(new Label("Display name"), 0, 2);
		this.optionsContent.add(new Label("Background RGB"), 0, 3);
		this.optionsContent.add(new Label("Text RGB"), 0, 4);
		this.optionsContent.add(new Label("Public Visible"), 0, 5);

		TextField codeNameField = new TextField();
		TextField displayNameField = new TextField();
		ObjColorInputPane backgroundColorField = new ObjColorInputPane();
		ObjColorInputPane textColorField = new ObjColorInputPane();
		CheckBox publicVisibleField = new CheckBox();

		this.optionsContent.add(codeNameField, 1, 1);
		this.optionsContent.add(displayNameField, 1, 2);
		this.optionsContent.add(backgroundColorField, 1, 3);
		this.optionsContent.add(textColorField, 1, 4);
		this.optionsContent.add(publicVisibleField, 1, 5);

		GridPane buttonBar = new GridPane();
		buttonBar.setHgap(5.0d);

		Button deleteButton = new Button("Delete");
		Button addButton = new Button("Add");
		Button saveButton = new Button("Save");

		deleteButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				UserFlag flag = flagsTable.getSelectionModel().getSelectedItem();
				if (flag != null) {
					flagsTable.getItems().remove(flag);
				}
			}
		});

		addButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				UserFlag flag = new UserFlag("new-flag", "Brand New Flag", new ObjColor(255, 255, 255), new ObjColor(0, 0, 0));
				flagsTable.getItems().add(flag);
				flagsTable.getSelectionModel().select(flag);
			}
		});

		saveButton.setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY) {
				UserFlag flag = flagsTable.getSelectionModel().getSelectedItem();
				if (flag != null) {
					flag.setCodeName(codeNameField.getText());
					flag.setDisplayName(displayNameField.getText());
					flag.setColorBack(backgroundColorField.getValue());
					flag.setColorFront(textColorField.getValue());
					flag.setPublicVisible(publicVisibleField.isSelected());
					flagsTable.refresh();
				}
				ArrayList<UserFlag> data = new ArrayList<>(flagsTable.getItems());
				try {
					JsonLoader.saveItems(data, "user-flags");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		this.flagsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				codeNameField.setText(newValue.getCodeName());
				displayNameField.setText(newValue.getDisplayName());
				backgroundColorField.setValue(newValue.getColorBack());
				textColorField.setValue(newValue.getColorFront());
				publicVisibleField.setSelected(newValue.getPublicVisible());
			}
		});

		buttonBar.addRow(0, deleteButton, addButton, saveButton);

		this.optionsContent.add(buttonBar, 0, 0, 2, 1);

		this.getItems().setAll(flagsTable, this.optionsPane);
	}
}

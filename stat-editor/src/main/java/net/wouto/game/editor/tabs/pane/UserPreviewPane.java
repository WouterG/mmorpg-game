package net.wouto.game.editor.tabs.pane;

import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import net.wouto.game.editor.obj.User;
import net.wouto.game.editor.obj.UserFlag;

public class UserPreviewPane extends GridPane {

	private Label usernameLabel;

	private GridPane flagsPane;

	private SimpleObjectProperty<User> value;

	public UserPreviewPane(User value) {
		this.value = new SimpleObjectProperty<>(value);
		this.initialize();
	}

	public SimpleObjectProperty<User> valueProperty() {
		return value;
	}

	private void initialize() {
		this.setBorder(new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID, new CornerRadii(10d), new BorderWidths(2d), new Insets(5d))));
		this.setBackground(new Background(new BackgroundFill(Color.GOLD, new CornerRadii(10d), new Insets(5d))));
		this.setPadding(new Insets(5d));
		this.usernameLabel = new Label();
		this.usernameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24.0d));
		this.flagsPane = new GridPane();
		this.add(this.usernameLabel, 0, 0);
		this.add(this.flagsPane, 0, 1);
		this.value.addListener((observable, oldValue, newValue) -> onValueChange());
	}

	public void update() {
		this.onValueChange();;
	}

	private void onValueChange() {
		this.usernameLabel.setText("");
		this.flagsPane.getChildren().clear();
		if (this.value.getValue() == null) {
			return;
		}
		this.usernameLabel.setText(this.value.getValue().getUsername());
		int index = 0;
		for (UserFlag userFlag : value.getValue().getFlags()) {
			this.flagsPane.add(new UserFlagElement(userFlag), 0, index);
			index++;
		}
	}

}

package net.wouto.game.editor.obj;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import net.wouto.game.editor.GameEditor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserFlagSerializer {

	public static class Deserializer extends  JsonDeserializer<List<UserFlag>> {

		public Deserializer() {
		}

		@Override
		public List<UserFlag> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			List<UserFlag> result = new ArrayList<>();
			JsonNode node = p.readValueAsTree();
			if (node.isArray()) {
				for (int i = 0; i < node.size(); i++) {
					JsonNode jsonNode = node.get(i);
					if (jsonNode.isTextual()) {
						String value = jsonNode.textValue();
						for (UserFlag userFlag : GameEditor.getInstance().getFlagsTable().getItems()) {
							if (userFlag.getCodeName() .equals(value)) {
								result.add(userFlag);
								break;
							}
						}
					}
				}
			}
			return result;
		}
	}

	public static class Serializer extends JsonSerializer<List<UserFlag>> {

		public Serializer() {
		}

		@Override
		public void serialize(List<UserFlag> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeStartArray();
			for (UserFlag userFlag : value) {
				gen.writeString(userFlag.getCodeName());
			}
			gen.writeEndArray();
		}
	}

}

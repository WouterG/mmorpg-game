package net.wouto.game.editor.json;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class JsonLoader {

	private static File itemFolder;

	private static ObjectMapper objectMapper;

	private static File getItemFolder() {
		if (itemFolder == null) {
			itemFolder = new File("./data");
			if (!itemFolder.exists()) {
				itemFolder.mkdirs();
			}
		}
		return itemFolder;
	}

	private static ObjectMapper getObjectMapper() {
		if (objectMapper == null) {
			objectMapper = new ObjectMapper();
			objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		}
		return objectMapper;
	}

	private static JavaType createTypeRef(Class prefix, JsonType type) {
		if (prefix != null) {
			type = new JsonType(prefix, type);
		}
		JavaType[] parameters = new JavaType[type.getParametric().size()];
		for (int i = 0; i < type.getParametric().size(); i++) {
			JsonType subtype = type.getParametric().get(i);
			parameters[i] = createTypeRef(null, subtype);
		}
		return getObjectMapper().getTypeFactory().constructParametrizedType(type.getType(), type.getTypeInterface(), parameters);
	}

	public static <T> List<T> loadItems(Class<T> type, String name) throws IOException {
		File f = new File(getItemFolder(), name + ".json");
		if (!f.exists()) {
			return Collections.emptyList();
		}
		System.out.println("loading from " + f.getName());
		return getObjectMapper().readValue(f, createTypeRef(null, new JsonType(ArrayList.class, Collection.class, new JsonType(type))));
	}

	public static void saveItems(List<?> items, String name) throws IOException {
		File orig = new File(getItemFolder(), name + ".json");
		File f = new File(getItemFolder(), name + ".json.new");
		if (f.exists()) {
			f.delete();
		}
		getObjectMapper().writeValue(f, items);
		if (orig.exists()) {
			if (orig.delete()) {
				f.renameTo(orig);
				System.out.println("wrote to " + orig.getName());
			}
		} else {
			f.renameTo(orig);
			System.out.println("wrote to " + orig.getName());
		}
	}

}

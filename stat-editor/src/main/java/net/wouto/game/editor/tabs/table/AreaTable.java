package net.wouto.game.editor.tabs.table;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.wouto.game.editor.obj.GameArea;

public class AreaTable extends TableView<GameArea> {

	public AreaTable() {
		this.initialize();
	}

	public AreaTable(ObservableList<GameArea> items) {
		super(items);
		this.initialize();
	}

	private void initialize() {
		TableColumn<GameArea, String> areaName = new TableColumn<>();
		areaName.setText("Area Name");
		areaName.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getName()));

		TableColumn<GameArea, Integer> ageCount = new TableColumn<>();
		ageCount.setText("Ages #");
		ageCount.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getAges().size()));

		this.getColumns().addAll(areaName, ageCount);
	}

}

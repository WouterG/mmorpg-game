package net.wouto.game.editor.obj;

import java.util.ArrayList;
import java.util.List;

public class GameArea {

	private String name;

	private List<GameAge> ages;

	public GameArea() {
		this("");
	}

	public GameArea(String name) {
		this.name = name;
		this.ages = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<GameAge> getAges() {
		return ages;
	}

}

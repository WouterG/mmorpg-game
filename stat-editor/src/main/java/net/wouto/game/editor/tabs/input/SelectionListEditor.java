package net.wouto.game.editor.tabs.input;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class SelectionListEditor<T> extends GridPane {

	private List<T> sourceItems;

	private Function<T, String> selector;

	private ObservableList<T> selectedList;

	private ListView<T> listViewSelected;

	private AutoCompleteTextField searchBox;

	private Map<String, T> lookupMap;

	public SelectionListEditor(List<T> sourceItems, Function<T, String> selector) {
		this.sourceItems = sourceItems;
		this.selector = selector;
		this.selectedList = FXCollections.observableArrayList(sourceItems);
		this.selectedList.clear();
		this.initialize();
	}

	public ObservableList<T> getSelectedList() {
		return selectedList;
	}

	private void initialize() {
		this.lookupMap = new HashMap<>();
		for (T sourceItem : this.sourceItems) {
			String text = this.selector.apply(sourceItem);
			lookupMap.put(text, sourceItem);
		}
		this.listViewSelected = new ListView<>(this.selectedList);
		this.listViewSelected.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.DELETE || event.getCode() == KeyCode.BACK_SPACE) {
				ArrayList<T> selected = new ArrayList<>(listViewSelected.getSelectionModel().getSelectedItems());
				if (selected.isEmpty()) {
					return;
				}
				listViewSelected.getSelectionModel().clearSelection();
				listViewSelected.getItems().removeAll(selected);
			}
		});
		this.listViewSelected.setCellFactory(param -> new ListCell<T>() {
			@Override
			protected void updateItem(T item, boolean empty) {
				if (item == null || empty) {
					setText("");
					super.updateItem(item, empty);
					return;
				}
				setText(selector.apply(item));
				super.updateItem(item, empty);
			}
		});
		this.searchBox = new AutoCompleteTextField();
		this.searchBox.getEntries().addAll(this.lookupMap.keySet());

		this.searchBox.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				String text = this.searchBox.getText();
				T value = lookupMap.get(text);
				if (value == null) {
					return;
				}
				this.listViewSelected.getItems().add(value);
				this.searchBox.setText("");
			}
		});

		this.add(this.searchBox, 0, 0);
		this.add(this.listViewSelected, 0, 1);
	}

}
